# BantayPanahon

## Current Situation
The NDRRMC disseminates notice to citizens regarding weather conditions via text and phone alerts to warn them and be ready. Providing insights before the event has been a good solution however, due to a large number of phone numbers that needs to be notified sometimes the alert arrive late.  And during the event, the LGUs; down to barangay level only relies to weather updates provided by DOST-PAGASA via television. Sometimes it doesn’t provide much data in their area of concern.

### Usage
For the purpose of this HANGATHON 2019, we will be just hitting the endpoint via Web Browser, Postman, curl or anything that can make request.

## Solution
A simple localized information dissemination application that checks the status (data) of Automatic Rain Gauge (ARG), Automated Weather Stations (AWS), Water Level Monitoring System (WLMS), Water Level with Rain Gauge (WLMS w/ ARG) every four (4) hours.
If the set threshold was met, it will trigger a process to send the current status to the assigned LGUs and Barangays. In that way, the citizen are up-to-date on the current situation not only relying to social media, radio, television.

## Assumption / Moving Forward
* Assuming we have the phone numbers, locations (coordinates at least) of recipients
* This can later be integrated to LUG’s Barangay Information System

## TechStack
* HTML/CSS/JS - for the Admin panel
* NodeJS/ExpressJS - for the backend
* MongoDB - for the datastore
* Nginx - for reverse-proxy and serving static files
* Docker - for containerization

## Hardware/API
* DOST-ASTI API - to get status of warning stations
* iTexMo API - for information dissemination (trial)
* ARG, AWS, WLMS (w/ ARG) - via DOST-ASTI


## Running the Application (Development)
##### Make sure you have **Docker** installed
* Go to project root folder
* Run this command from the terminal `docker-compose up --build` (for the first time)