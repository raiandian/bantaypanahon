const mongoose = require("mongoose");

const deviceSchema = mongoose.Schema({
  devId: { type: Number, required: true, trim: true },
  location: { type: String, required: true, trim: true },
  province: { type: String, required: true, trim: true},
  type: { type: String, required: true, trim: true },
  benchmark: { type: Number, trim: true }
});

module.exports = mongoose.model("Device", deviceSchema);
