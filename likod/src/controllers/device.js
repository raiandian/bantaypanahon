const Device = require('../models/device')

const log4js = require('log4js')
const axios = require('axios')

log4js.configure('./log-config.json')
const logger = log4js.getLogger('ControllerDevice')

// prepared response
const notFound = "We could not find what you are requesting!"

exports.GetAll = async (req, res) => {

  Device
    .find()
    .then(result => {
      if (result) {
        res.status(200).json(result)
        logger.info('GetAll:', result)
      } else {
        res.status(404).json({ message: notFound })
        logger.warn('GetAll:', notFound)
      }
    })
    .catch (error => {
      res.status(500).json()
      logger.error('GetAll:', error)
    })

}

exports.GetOne = async (req, res) => {

  try {
    const response = await axios.get(`${process.env.ASTI_API}/${req.params.id}`)
    res.status(200).json(response.data)
    logger.info(`GetOne: ${req.params.id}`, response.data)
  } catch (error) {
    res.status(500).json()
    logger.error(`GetOne: ${req.params.id}`, error)
  }

}

exports.GetAllId = async (req, res) => {

  try {
    Device
      .find()
      .select('devId')
      .then(result => {
        if (result) {
          res.status(200).json(result)
          logger.info('GetAllToSms:', result)
        } else {
          res.status(404).json({ message: notFound })
          logger.warn('GetAllToSms:', notFound)
        }
      })
      .catch (error => {
        res.status(500).json()
        logger.error('GetAllToSms:', error)
      })
  } catch (error) {
    res.status(500).json()
    logger.error(`GetAllId: ${error}`)
  }

}