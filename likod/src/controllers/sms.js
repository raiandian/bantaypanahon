const Device = require('../models/device')

const log4js = require('log4js')
const axios = require('axios')
const _ = require('lodash')

log4js.configure('./log-config.json')
const logger = log4js.getLogger('ControllerSms')

// prepared response
const notFound = "We could not find what you are requesting!"

const runGeoQuery = (lng, lat, res) => {
  if (isNaN(lng) || isNaN(lat)) {
    const message = "Invalid coordinates!";
    res.status(400).json({ message: message })
    logger.warn(message)
    return
  }

  const point = { type: "Point", coordinates: [lng, lat] }
  const geoOptions = { spherical: true, maxDistance: 10000 }

  // Device
  //   .geoNear(point, geoOptions, ())
};

exports.SendSms = async (req, res) => {

  /**
   * check - Get the device IDs
   * Get each device last 4 data
   * Run GeoNear to Get target recipients
   * Send the SMS containing the following information/format
   * (date - time) The current rainfall data */

  try {

    let wx = []
    let axiosRequests = []

    logger.trace('Requesting Device IDs to /api/v1/devices/send')

    const devices = await axios.get(
      `${process.env.APP_HOST}/v1/devices/send`,
      {
        proxy: {
          host: '127.0.0.1',
          port: 3000
        }
      }
    )

    _.forEach(devices.data, data => {

      let newPromiseRequest = axios({
        method: 'get',
        url: `${process.env.APP_HOST}/v1/devices/${data.devId}`,
        proxy: {
          host: '127.0.0.1',
          port: 3000
        }
      })
      axiosRequests.push(newPromiseRequest)

    })

    axios
      .all(axiosRequests)
      .then(axios.spread((...responses) => {
        wx = responses.forEach(results => logger.info(`Yes`));
      }))

    // _.forEach(draftIds.data, data => {
    //   wx = await axios
    //     .get(`${process.env.APP_HOST}/v1/devices/${data.devId}`, {
    //       proxy: {
    //         host: "127.0.0.1",
    //         port: 3000
    //       }
    //     })
    //     // .then(draftDevData => {
    //     //   wx = draftDevData.data.data;
    //     //   logger.info("Just info", draftDevData.data.data);
    //     // })

    //   // logger.info('Requesting Device Data', draftDeviceData)
    // })

    res.status(200).json(wx)

  } catch (error) {
    res.status(500).json();
    logger.error(`SendSms: ${error}`)
  }

}