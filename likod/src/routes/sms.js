const express = require('express')

const SmsController = require('../controllers/sms')

const router = express.Router()

router.get('/send', SmsController.SendSms)

module.exports = router
