const express = require('express')

const DeviceController = require('../controllers/device')

const router = express.Router()

router.get('', DeviceController.GetAll)
router.get('/send', DeviceController.GetAllId);
router.get('/:id', DeviceController.GetOne)

module.exports = router