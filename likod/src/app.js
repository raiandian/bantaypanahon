const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const rateLimit = require('express-rate-limit')
const log4js = require('log4js')

const deviceRoute = require('./routes/device')
const smsRoute = require('./routes/sms')

log4js.configure('./log-config.json')
const logger = log4js.getLogger('App')
const app = express()
const limiter = rateLimit({ windowMs: 1000, max: 2 })

mongoose
  .connect(
    `mongodb://${process.env.DB_USER}:${process.env.DB_PWD}@${process.env.DB_URL}:${process.env.DB_PORT}/${process.env.DB_NAME}`,
    {
      authSource: 'admin',
      autoReconnect: true,
      connectTimeoutMS: 3000,
      reconnectInterval: 1000,
      reconnectTries: 50,
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
  )
  .then(() => logger.info("Connected to the database!"))
  .catch((error) => logger.info("Cannot establish connection to database!", error))
mongoose.connection.on('disconnected', () => logger.fatal('Disconnected from the Database Server'))
mongoose.connection.on('reconnect', () => logger.trace('Reconnecting to Database Server'))
mongoose.connection.on('reconnected', () => logger.info('Reconnected from the Database Server'))

app.use(limiter)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Request-With, Content-Type, Accept, Authorization')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS')
  next()
})

app.use('/api/v1/devices', deviceRoute)
app.use('/api/v1/sms', smsRoute)

module.exports = app;