const app = require('./src/app')
const http = require('http')
const log4js = require('log4js')

log4js.configure('./log-config.json')
const logger = log4js.getLogger('Server');

// Configure normalizing port
const normalizePort = val => {

  const port = parseInt(val, 10)
  if (isNaN(port)) { return val }
  if (port >= 0) { return port }
  return false

}

const onError = error => {

  if (error.syscall !== 'listen') { throw error }

  const bind = typeof addr === 'string' ? 'pipe' + addr : 'port' + port

  switch (error.code) {
    case 'EACCES':
      logger.fatal(`${bind} required elevated privileges`)
      process.exit(1)
      break;
    case 'EADDRINUSE':
      logger.fatal(`${bind} is already in use`)
      process.exit(1)
      break
    default:
      throw error
  }

}

const onListening = () => {

  const addr = server.address()
  const bind = typeof addr === "string" ? "pipe" + addr : "port " + port
  logger.info(`Listening on ${bind}`)
}

const port = normalizePort(process.env.PORT)
app.set('port', port)
const server = http.createServer(app)
server.on('error', onError)
server.on('listening', onListening)
server.listen(port)

logger.info(`Connected to port ${port}`)